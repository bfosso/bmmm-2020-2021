# RELAZIONE
  
Ogni studente dovrà presentare una relazione relativa ai laboratori svolti entro **5gg LAVORATIVI** dalla data in cui è previsto l’esame orale.  
Esempio: Prendiamo ad esempio l'esame previsto per il lunedì 08/02.   
 
|02 Febbraio|03 Febbraio|04 Febbraio|05 Febbraio|06 Febbraio|07 Febbraio|08 Febbraio|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Martedì|Mercoledì|Giovedì|Venerdì|Sabato|Domenica|Lunedì|
|LAVORATIVO|LAVORATIVO|LAVORATIVO|LAVORATIVO|NON LAVORATIVO|NON LAVORATIVO|**ESAME**|
|**LIMITE ULTIMO PER LA CONSEGNA**|4|3|2|1|1|1|

La relazione dovrà riguardare **tutte** le seguenti tematiche:
* Descrizione della procedura bioinformatica per l’assemblaggio e annotazione di un genoma batterico corredata da un breve commento dei risultati ottenuti;
  
* Descrizione della procedura bioinformatica relativa all’analisi di dati di Metabarcoding e discussione dei dati ottenuti:  
    - Vanno comparati i risultati ottenuti durante l'esercitazione con quelli ottenuti nel lavoro Ravegnini et al. 2020; 
    - Inoltre, va effettuata una ricerca in **PubMed** relativa al ***Fusobacterium nucleatum*** e alla possibile associazione con patologie: selezionarne e discuterne uno.  
    - Nella relazione va indicata la ricerca effettuata su **PubMed** e il PMID del lavoro scelto.  
    
* Nella tabella sottostante è indicato per ciascuno di voi un gene umano per cui dovrà effettuare la ricerca su **ENSEMBL** e indicare le seguenti informazioni:
    - Simbolo ufficiale del Gene;   
    - Identificativo del Gene;
    - Localizzazione cromosomica comprensiva di coordinate e strand;  
    - Numero di trascritti annotati;
        - Considerando il trascritto codificante proteina più lungo dovete indicare:
            * l'identificativo del trascritto;
            * il numero di Esoni che partecipano alla formazione di quel trascritto;
            * la lunghezza delle regione 5' e 3' UTR e della CDS;
            * la lunghezza della proteina codificata.    
    - Numero di Ortologhi annotati.  
    - Utilizzando la sequenza proteica identificate la presenza di possibile proteine omologhe.


|Nome|Cognome|Gene Name|
|:---:|:---:|:---:|
|Nome |Cognome|Name|
|Paulina|Borowiec|alpha-1-B glycoprotein|
|Rosa|Buonamassa|arylacetamide deacetylase like 2|
|Diletta|Capobianco|ATP binding cassette subfamily C member 1|
|Carmela|Centoducati|ATP binding cassette subfamily G member 5|
|Antonella|Cicirelli|acetylcholinesterase (Cartwright blood group)|
|Sara |Cileo|apoptotic chromatin condensation inducer 1|
|Carmela |Colabufo|atypical chemokine receptor 1 (Duffy blood group)|
|Francesca|Conte|acyl-CoA thioesterase 2|
|Cristiano|Coppola|ADAM metallopeptidase domain 9|
|Angelica|D'Ascoli|1-acylglycerol-3-phosphate O-acyltransferase 3|
|Anna Simona|Daddario|aldo-keto reductase family 1 member D1|
|Giulia|de Martino|ALG10 alpha-1,2-glucosyltransferase|
|Katya|De Meo|apolipoprotein C3|
|Giuseppe|Debiase|keratin 77|
|Ilaria|Delle Fontane|Kell metallo-endopeptidase (Kell blood group)|
|Giuseppe|Di Noia|kynurenine 3-monooxygenase|
|Claudia|Dibenedetto|keratin associated protein 9-4|
|Ilaria|Ferro|laminin subunit gamma 1|
|Giorgia |Gemma |lymphocyte activating 3|
|Desirè |Greco|ladinin 1|
|Rebecca |Lasalandra|lactamase beta like 1|
|Nicoletta|Lionetti |L3MBTL histone methyl-lysine binding protein 1|
|Sabrina|Luchena|late cornified envelope 3D|
|Federica|Mafrica|lipocalin 2|
|Roberta|Maggiore|leucine rich repeat containing G protein-coupled receptor 4|
|Alessio|Manfredi|luteinizing hormone/choriogonadotropin receptor|
|Sara |Massa|lengsin, lens protein with glutamine synthetase domain|
|Angela|Matarrese|microtubule actin crosslinking factor 1|
|Ciro|Mazza|macrophage erythroblast attacher, E3 ubiquitin ligase|
|Alessia|Memeo|MAGE family member C2|
|Isabella|Mongelli|microtubule associated serine/threonine kinase 1|
|Francesca|Monteleone|methylmalonyl-CoA epimerase|
|Chiara|Morrone|midkine|
|Marika|Pagliara|Mdm1 nuclear protein|
|elena|paternoster|matrix remodeling associated 8|
|Valeria|Perniola|tripartite motif containing 38|
|Paola |Poliseno |TruB pseudouridine synthase family member 1|
|Giuseppe|Romanazzi|laminin subunit beta 3|
|Silvia |Saltarelli|TSC complex subunit 1|
|Salvatore Alessandro|Santeramo|tubulin alpha 1a|
|Giorgia |Sclavo |U2 small nuclear RNA auxiliary factor 2|
|Elena|Serra|ubinuclein 1|
|Miriana|Sicolo|UDP-glucose 6-dehydrogenase|
|Gianluca|Signorile|cAMP responsive element binding protein 3 like 4|
|A.Federica|Sirago|cAMP responsive element modulator|
|Elena|Squiccimarro|CDP-L-ribitol pyrophosphorylase A|
|Valeria|Vurchio|cell cycle regulator of NHEJ|
|Annamaria |Zerulo|DDB1 and CUL4 associated factor 6|


[Back to the top](../README.md) 