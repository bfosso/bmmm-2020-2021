# **ESERCITAZIONI PRATICHE DEL CORSO DI BIOINFORMATICA ED ANALISI FUNZIONALE DEL GENOMA**  

Prima di iniziare, è il caso di conoscersi un po': [***QUESTIONARIO***](https://www.menti.com/9awgurbzuo).  


## Programma
### Esercitazione 1 (16/11/2020)
#### Banche dati: definizioni generali e sistemi di retrieval
- [X] [Introduzione alla Bioinformatica](./Intro_Bioinfo.md)
- [X] [Definizione di Database e struttura delle entry **GenBank**](Banche_dati/banche_dati.md)  
- [X] [Sistemi di retrieval](Banche_dati/retrieval.md)  
- [X] [PubMed](Banche_dati/pubmed.md)  
- [X] [GISAID](Banche_dati/gisaid.md)  

### Esercitazione 2 (30/11/2020)
#### Allineamento tra sequenze
- [X] [Allineamento](./Allineamento_tra_sequenze/allineamento.md)
- [X] [Utilizzo di BLAST e BLAT per l'analisi genomica](./Allineamento_tra_sequenze/blast_genomico.md)
#### Genome Browser
- [X] [ENSEMBL](./Genome_Browser/Ensembl.md)

### Esercitazione 3 (08/01/2021)
- [X] [ENSEMBL: esercizi](./Genome_Browser/Ensembl.md#esercizi)
- [X] [ENSEMBL: COVID-19](./Genome_Browser/Ensembl.md#covid-19)
- [X] [UCSC](./Genome_Browser/UCSC.md)
- [X] [Connessione al server ed introduzione al BASH](./BASH/Accesso_al_server.md)  

### Esercitazione 4 (11-13/01/2021)
### Metabarcoding
- [X] [Ancora BASH...:fearful: :fearful: :fearful: ](./BASH/Accesso_al_server.md#il-formato-fasta)  


### Esercitazione 5 - 6 (13-15-18/01/2021)
### Metabarcoding / Genome Assembly
- [ ] [Introduzione al Metabarcoding](./Metabarcoding/metabarcoding.md)
- [ ] [Genome Assembly](./Genome_Assembly/genome_assembly.md)

### [Relazioni](./relazioni.md)  
